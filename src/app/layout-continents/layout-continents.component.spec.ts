import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutContinentsComponent } from './layout-continents.component';

describe('LayoutContinentsComponent', () => {
  let component: LayoutContinentsComponent;
  let fixture: ComponentFixture<LayoutContinentsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutContinentsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutContinentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
