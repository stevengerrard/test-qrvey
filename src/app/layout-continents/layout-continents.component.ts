import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';

import { CountriesService } from '../services/countries.service';

@Component({
  selector: 'app-layout-continents',
  templateUrl: './layout-continents.component.html',
  styleUrls: [ './layout-continents.component.scss' ]
})
export class LayoutContinentsComponent implements OnInit, OnDestroy {

  public countryList: any[] = [];
  public auxCountryList: any[] = [];
  public continentList: any[][] = [];
  public favoriteCountries: any[] = [];
  public countryInfoModal: any;
  public showModal = false;
  public showStar = false;

  private dataFilter: any;
  private listNameCountries: any[] = [];
  private subsGetCountries: Subscription = new Subscription();

  constructor(
    private countriesService: CountriesService
  ) { }

  /**
   * @description
   * Función que obtiene la información del pais, para ver su detalle
   * @param country Contiene un objeto con la información del pais
   */
  onGetInfoCountry(country: any): void {

    if (country.detail?.borders) {
      country.detail.bordersFullName = this.borderFullName(country.detail.borders);
    }

    country.detail.favorite = this.checkCountryInFavorite(country.detail);
    country.detail.capitalName = country.detail.capital ? country.detail.capital[0] : 'No Capital';

    this.countryInfoModal = country.detail;
    this.showModal = true;
  }

  /**
   * @description
   * Función que cierra el modal
   */
  onCloseModal(): void {
    this.showModal = false;
  }

  /**
   * @description
   * Función que obtiene la información del filtro
   * @param data Contiene un objeto con la información del filtro
   */
  onGetInfoFilter(data: any): void {

    this.dataFilter = data;

    if (data.region === 'all') {
      this.groupCountriesByContinents(this.countryList);
      this.showStar = false;
    } else if (data.region === 'fav') {
      this.showStar = true;
      this.auxCountryList = this.filterCountriesByFavorite();
      this.groupCountriesByContinents(this.auxCountryList);
    } else {
      this.showStar = false;
      this.auxCountryList = this.filterCountriesByRegion(data.region);
      this.groupCountriesByContinents(this.auxCountryList);
    }
  }

  /**
   * @description
   * Función que obtiene un nombre de un pais para buscarlo por nombre
   * @param nameCountry Contiene un string
   */
  onGetNameCountryBySearch(nameCountry: string): void {
    this.auxCountryList = this.filterCountriesByName(nameCountry);
    this.groupCountriesByContinents(this.auxCountryList);
  }

  /**
   * @description
   * Función que obtiene la información del pais que será agregado o quitado de favorito
   * @param country Contiene un objeto con la información del pais
   */
  onGetSwitchFavorite(country: any): void {

    if (country.detail.favorite) {
      this.addToFavoriteCountry(country.detail);
    } else {
      this.removeToFavoriteCountry(country.detail);
    }
  }

  ngOnInit(): void {
    this.getCountries();
    // this.favoriteCountries = this.countriesService.getFavoriteCountries();
  }

  ngOnDestroy(): void {
    this.subsGetCountries.unsubscribe();
  }

  /**
   * @description
   * Función que obtiene los paises
   */
  private getCountries(): void {

    this.subsGetCountries = this.countriesService.getCountries()
      .subscribe((countries: any[]) => {

        this.listNameCountries = countries
          .map((country: any) => ({ name: country.name.common, cca3: country.cca3 }));

        this.countryList = countries;
        this.auxCountryList = countries;

        this.groupCountriesByContinents(countries);
      });
  }

  /**
   * @description
   * Función que agrupa los paises por continente
   */
  private groupCountriesByContinents(countriesList: any[]): void {

    this.continentList = [];

    let country: any = null;
    let countriesByContinent: any[][] = [];

    while (countriesList.length > 0) {

      country = countriesList[ 0 ];

      countriesByContinent = countriesList.filter((itemCountry: any) => itemCountry.region === country.region).sort();

      countriesList = countriesList.filter((itemCountry: any) => itemCountry.region !== country.region);

      countriesByContinent = this.sortCountries(countriesByContinent);

      this.continentList.push(countriesByContinent);
    }

    this.continentList = this.sortContinents(this.continentList);
  }

  /**
   * @description
   * Función que ordena los paises por nombre
   * @param countries Contiene un array de tipo any
   * @returns Un array de tipo any
   */
  private sortCountries(countries: any[]): any[] {
    return countries.sort((a: any, b: any) => {
      if (a.name.common > b.name.common) {
        return 1;
      }

      return -1;
    });
  }

  /**
   * @description
   * Función que ordena los paises por nombre
   * @param contienens Contiene un array de tipo any
   * @returns Un array de tipo any
   */
  private sortContinents(contienens: any[]): any[] {

    return contienens.sort((a: any, b: any) => {
      if (a[ 0 ].region > b[ 0 ].region) {
        return 1;
      }

      return -1;
    });
  }

  /**
   * @description
   * Función que obtiene los nombres completos de los borders
   * @param borders Contiene un array de string
   * @returns Un array de string
   */
  private borderFullName(borders: string[]): any[] {

    borders = borders
      .filter((border: any) => border)
      .map((border: any) => {
        border = this.listNameCountries.find((country: any) => country.cca3 === border)?.name;
        return border;
      });

    return borders;
  }

  /**
   * @description
   * Función que filtra los paises por región
   * @param region Contiene un string
   * @returns Un array de tipo any
   */
  private filterCountriesByRegion(region: any): any[] {
    const countries = this.countryList.filter((country: any) => country.region === region);
    return countries;
  }

  /**
   * @description
   * Función que filtra los paises por nombre
   * @param nameCountry Contiene un string
   * @returns Un array de tipo any
   */
  private filterCountriesByName(nameCountry: string): any[] {

    if (nameCountry) {

      const countries = this.auxCountryList
        .filter((country: any) => country.name.common.toLowerCase().startsWith(nameCountry.toLowerCase()));

      return countries;
    } else if (this.dataFilter && this.dataFilter.region) {

      if (this.dataFilter.region === 'fav') {
        return this.filterCountriesByFavorite();
      } else if (this.dataFilter.region === 'all') {
        return this.countryList;
      }

      return this.filterCountriesByRegion(this.dataFilter.region);
    }

    return this.countryList;
  }

  /**
   * @description
   * Función que filtra los paises favoritos
   */
  private filterCountriesByFavorite(): any[] {
    return this.favoriteCountries = this.countriesService.getFavoriteCountries();
  }

  /**
   * @description
   * Función que agrega un pais a favorito
   * @param country Contiene un objeto con la información del pais
   */
  private addToFavoriteCountry(country: any): void {
    this.favoriteCountries.push(country);

    if (this.dataFilter && this.dataFilter.region === 'fav') {
      this.groupCountriesByContinents(this.favoriteCountries);
    }

    this.countriesService.updateFavoriteData(this.favoriteCountries);
  }

  /**
   * @description
   * Función que quita un pais de favorito
   * @param country Contiene un objeto con la información del pais
   */
  private removeToFavoriteCountry(country: any): void {

    const index = this.favoriteCountries.findIndex((itemCountry: any) => itemCountry.name.common === country.name.common);

    this.favoriteCountries.splice(index, 1);

    if (this.dataFilter && this.dataFilter.region === 'fav') {
      this.groupCountriesByContinents(this.favoriteCountries);
    }

    this.countriesService.updateFavoriteData(this.favoriteCountries);
  }

  /**
   * @description
   * Función que verifica si el pais está en favoritos, esto se hace para
   * asignar a la propiedad favorite true
   * @param country Contiene un objeto con la información del pais
   * @returns Un boolean
   */
  private checkCountryInFavorite(country: any): boolean {

    const check = this.favoriteCountries.some((itemCountry: any) => itemCountry.name.common === country.name.common);

    if (check) {
      return true;
    }

    return false;
  }
}
