import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-find-country',
  templateUrl: './find-country.component.html',
  styleUrls: [ './find-country.component.scss' ]
})
export class FindCountryComponent implements OnInit {

  @Output() sendDataForm = new EventEmitter<any>();
  @Output() sendCountryName = new EventEmitter<string>();


  public searchCountry = this.fb.group({
    countryName: [ '' ],
    region: [ '' ]
  });

  filterList = [
    { name: 'show all', value: 'all' },
    { name: 'favorites', value: 'fav' },
    { name: 'africa', value: 'Africa' },
    { name: 'america', value: 'Americas' },
    { name: 'Antarctic', value: 'Antarctic' },
    { name: 'asia', value: 'Asia' },
    { name: 'europe', value: 'Europe' },
    { name: 'oceania', value: 'Oceania' },
  ];


  constructor(
    private fb: FormBuilder
  ) { }

  /**
   * @description
   * Función que emite la información del formulario
   */
  onSendDataForm(): void {
    this.sendDataForm.emit(this.searchCountry.value);
  }

  onSendCountryName(): void {
    const countryName = this.searchCountry.get('countryName')?.value;
    this.sendCountryName.emit(countryName);
  }

  ngOnInit(): void {
  }

}
