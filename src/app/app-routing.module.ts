import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutContinentsComponent } from './layout-continents/layout-continents.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutContinentsComponent
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
