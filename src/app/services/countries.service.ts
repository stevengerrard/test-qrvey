import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CountriesService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * @description
   * Función que obtiene los paises
   * @returns Un observable de tipo any
   */
  getCountries(): Observable<any> {
    return this.http.get('https://restcountries.com/v3/all');
  }

  /**
   * @description
   * FUnción que guarada los paises favoritos en sessionStorage
   * @param countries Contiene un array de objetos con la infomación de los paises
   */
  updateFavoriteData(countries: any[]): void {
    localStorage.setItem('favoriteCountries', JSON.stringify(countries));
  }

  /**
   * @description
   * Función que obtiene los paises de favorito
   */
  getFavoriteCountries(): any[] {
    const countries = localStorage.getItem('favoriteCountries');
    if (countries) { return JSON.parse(countries); }
    return [];
  }
}
